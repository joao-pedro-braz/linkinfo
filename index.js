"use strict";

const debug = false;

const openLinkInNewTabSvgURL = (
  typeof browser !== "undefined" ? browser : window.chrome
).runtime.getURL("assets/open_link_in_new_tab.svg");

const getOpenLinkInNewTabSvgFactory = () => {
  let result = null;

  return async () => {
    if (result) return result;

    result = await fetch(openLinkInNewTabSvgURL)
      .then(async (res) => {
        const parser = new DOMParser();
        const svg = document.createElement("svg");
        svg.appendChild(
          parser.parseFromString(await res.text(), "image/svg+xml").firstChild
        );
        svg.style.display = "inline-block";
        svg.style.width = "0.8em";
        return svg;
      })
      .catch(console.error);

    return result;
  };
};

const getOpenLinkInNewTabSVG = getOpenLinkInNewTabSvgFactory();

let currentActiveElement = null;

const isAnchorTagWithTargetBlank = (el) =>
  el?.nodeName === "A" && el?.target === "_blank";

const anchorAlreadyHasInfo = (el) =>
  el?.getElementsByTagName("SVG").length > 0 ||
  el?.getElementsByTagName("IMG").length > 0;

const removeDecorationFromActiveAnchor = async () => {
  debug && console.log("[REMOVE]", currentActiveElement);

  const svg = await getOpenLinkInNewTabSVG();
  currentActiveElement.removeChild(svg);
  currentActiveElement = null;
};

const decorateActiveAnchor = async () => {
  debug && console.debug("[DECORATE]", currentActiveElement);

  const svg = await getOpenLinkInNewTabSVG();
  currentActiveElement.appendChild(svg);
};

const handleBodyMouseover = (e) => {
  const { clientX, clientY } = e;
  const currentElement = document.elementFromPoint(clientX, clientY);

  if (currentActiveElement && currentElement !== currentActiveElement)
    removeDecorationFromActiveAnchor();

  if (
    currentElement === currentActiveElement ||
    !isAnchorTagWithTargetBlank(currentElement) ||
    anchorAlreadyHasInfo(currentElement)
  )
    return;

  currentActiveElement = currentElement;

  decorateActiveAnchor();
};

document.body.addEventListener("mouseover", handleBodyMouseover);
